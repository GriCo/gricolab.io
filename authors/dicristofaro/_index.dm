---
# Display name
name: Matteo Di Cristofaro

# Username (this should match the folder name)
authors:
- dicristofaro

# Is this the primary user of the site?
superuser: false

# Role/position
role: Researcher, Lecturer

# Organizations/Affiliations
organizations:
- name: Università degli Studi di Modena e Reggio Emilia
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include language analysis, cognitive sciences, and Artificial Intelligence.

interests:
- Corpus Linguistics
- Computational Linguistics
- Cognitive Linguistics
- CADS (Corpus-Assisted Discourse Studies)
- Data Analysis
- FLOSS
- Continental Philosophy

education:
  courses:
  - course: PhD in Corpus and Cognitive Linguistics
    institution: Lancaster University
    year: 2015
  - course: MA in Languages
    institution: Università degli studi di Modena e Reggio Emilia
    year: 2010
  - course: BA in Languages
    institution: Università degli studi di Modena e Reggio Emilia
    year: 2007

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
#- icon: envelope
#  icon_pack: fas
#  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: user-circle
  icon_pack: fas
  link: https://infogrep.it
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/matteodic
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=v5x_BO8AAAAJ&hl=en&oi=ao
- icon: github
  icon_pack: fab
  link: https://github.com/mdic
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/matteodc/
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0001-7027-2894
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/matteo-di-cristofaro/
- icon: telegram
  icon_pack: fab
  link: https://t.me/infogrep
  
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers

---


My fascination with Language and IT led me to become a linguist, specialised in Corpus and Cognitive Linguistics, and CADS (Corpus-Assisted Discourse Studies).  
  
Having worked in [several interdisciplinary projects](#tags) I developed methods and tools to collaborate with different disciplines - from the project design to the data analysis, using Free/Libre and Open Source Software ([FLOSS](//https://www.gnu.org/philosophy/floss-and-foss.en.html)) - providing a linguistic perspective to better understand the data under investigation.  
  
I actively collaborate with the *civic factory* [OvestLab](//www.ovestlab.it), applying language and data analysis to projects concerning social inclusion, civic innovation, and urban regeneration.
